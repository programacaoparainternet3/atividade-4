﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gastos.Models
{
    public class Receita
    {
        public long Id { get; set; }
        public Double Valor { get; set; }
    }
}
