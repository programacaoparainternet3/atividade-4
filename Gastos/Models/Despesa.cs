﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gastos.Models
{
    public class Despesa
    {
        public long Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public Double Valor { get; set; }
        public Boolean Pessoal { get; set; }
        public Categoria Categoria { get; set; }
        public long CategoriaId { get; set; }

        public Despesa()
        {
        }

        public Despesa(long id, Double valor, Categoria categoria, bool pessoal)
        {
            Id = id;
            Valor = valor;
            Categoria = categoria;
            Pessoal = pessoal;
        }
    }
}
