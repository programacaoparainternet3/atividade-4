﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gastos.Models
{
    public class Cartao
    {
        public long Id { get; set; }
        public int DigitosFinais { get; set; }
        public String Bandeira { get; set; }

    }
}
