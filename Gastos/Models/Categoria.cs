﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gastos.Models
{
    public class Categoria
    {
        public long Id { get; set; }
        public String Nome { get; set; }

        public ICollection<Despesa> Despesas { get; set; }

        public Categoria()
        {
        }

        public Categoria(long id, string nome)
        {
            Id = id;
            Nome = nome;
        }

        public void AdicionarDespesa(Despesa despesa)
        {
            Despesas.Add(despesa);
        }

        public void RemoverDespesa(Despesa despesa)
        {
            Despesas.Remove(despesa);
        }
    }
}
