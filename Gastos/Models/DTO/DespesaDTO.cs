﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gastos.Models.DTO
{
    public class DespesaDTO
    {
        public Despesa Despesa { get; set; }
        public List<Categoria> Categorias { get; set; }
    }
}
