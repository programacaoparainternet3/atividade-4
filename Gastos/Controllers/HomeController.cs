﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gastos.Models;
using Gastos.Data;

namespace Gastos.Controllers
{
    public class HomeController : Controller
    {
        private readonly GastosContext _context;

        public HomeController(GastosContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            Double TotalReceitas = 0;
            Double TotalDespesas = 0;
            List<Despesa> Despesas = _context.Despesa.ToList();
            List<Receita> Receitas = _context.Receita.ToList();
            foreach (Despesa obj in Despesas)
            {
                TotalDespesas += obj.Valor;
            }
            foreach (Receita obj in Receitas)
            { 
                TotalReceitas += obj.Valor;
            }
            ViewBag.TotalReceitas = TotalReceitas.ToString("N2");
            ViewBag.TotalDespesas = TotalDespesas.ToString("N2");
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
