﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Gastos.Data;
using Gastos.Models;

namespace Gastos.Controllers
{
    public class CartoesController : Controller
    {
        private readonly GastosContext _context;

        public CartoesController(GastosContext context)
        {
            _context = context;
        }

        // GET: Cartoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Cartao.ToListAsync());
        }

        // GET: Cartoes/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cartao = await _context.Cartao
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cartao == null)
            {
                return NotFound();
            }

            return View(cartao);
        }

        // GET: Cartoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cartoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DigitosFinais,Bandeira")] Cartao cartao)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cartao);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cartao);
        }

        // GET: Cartoes/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cartao = await _context.Cartao.FindAsync(id);
            if (cartao == null)
            {
                return NotFound();
            }
            return View(cartao);
        }

        // POST: Cartoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,DigitosFinais,Bandeira")] Cartao cartao)
        {
            if (id != cartao.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cartao);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CartaoExists(cartao.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cartao);
        }

        // GET: Cartoes/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cartao = await _context.Cartao
                .FirstOrDefaultAsync(m => m.Id == id);
            if (cartao == null)
            {
                return NotFound();
            }

            return View(cartao);
        }

        // POST: Cartoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var cartao = await _context.Cartao.FindAsync(id);
            _context.Cartao.Remove(cartao);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CartaoExists(long id)
        {
            return _context.Cartao.Any(e => e.Id == id);
        }
    }
}
