﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Gastos.Data;
using Gastos.Models;
using Gastos.Models.DTO;

namespace Gastos.Controllers
{
    public class DespesasController : Controller
    {
        private readonly GastosContext _context;
        
        public DespesasController(GastosContext context)
        {
            _context = context;
        }

        // GET: Despesas
        public async Task<IActionResult> Index()
        {
            Double Total = 0;
            List<Despesa> Despesas = await _context.Despesa.ToListAsync();
            foreach(Despesa obj in Despesas)
            {
                obj.Categoria = _context.Categoria.Where(x => x.Id == obj.CategoriaId).First();
                Total += obj.Valor;
            }
            ViewBag.Total = Total.ToString("N2");
            return View(Despesas);
        }

        // GET: Despesas/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var despesa = await _context.Despesa
                .FirstOrDefaultAsync(m => m.Id == id);
            if (despesa == null)
            {
                return NotFound();
            }
            despesa.Categoria = _context.Categoria.Where(x => x.Id == despesa.CategoriaId).First();
            return View(despesa);
        }

        // GET: Despesas/Create
        public IActionResult Create()
        {
            return View(new DespesaDTO{ Categorias = _context.Categoria.ToList() });
        }

        // POST: Despesas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Despesa despesa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(despesa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(despesa);
        }

        // GET: Despesas/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var despesa = await _context.Despesa.FindAsync(id);
            if (despesa == null)
            {
                return NotFound();
            }
            return View(new DespesaDTO { Despesa = despesa, Categorias = _context.Categoria.ToList() });
        }

        // POST: Despesas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, DespesaDTO despesaParam)
        {
            Despesa despesa = despesaParam.Despesa;
            if (id != despesa.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(despesa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DespesaExists(despesa.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(new DespesaDTO { Despesa = despesa, Categorias = _context.Categoria.ToList() });
        }

        // GET: Despesas/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var despesa = await _context.Despesa
                .FirstOrDefaultAsync(m => m.Id == id);
            if (despesa == null)
            {
                return NotFound();
            }

            return View(new DespesaDTO { Despesa = despesa, Categorias = _context.Categoria.ToList() });
        }

        // POST: Despesas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var despesa = await _context.Despesa.FindAsync(id);
            _context.Despesa.Remove(despesa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DespesaExists(long id)
        {
            return _context.Despesa.Any(e => e.Id == id);
        }
    }
}
