﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Gastos.Models;

namespace Gastos.Data
{
    public class GastosContext : DbContext
    {
        public GastosContext (DbContextOptions<GastosContext> options)
            : base(options)
        {
        }

        public DbSet<Gastos.Models.Categoria> Categoria { get; set; }

        public DbSet<Gastos.Models.Cartao> Cartao { get; set; }

        public DbSet<Gastos.Models.Despesa> Despesa { get; set; }

        public DbSet<Gastos.Models.Receita> Receita { get; set; }
    }
}
