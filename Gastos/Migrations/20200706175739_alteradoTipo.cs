﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Gastos.Migrations
{
    public partial class alteradoTipo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Valor",
                table: "Despesa",
                nullable: false,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Valor",
                table: "Despesa",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
